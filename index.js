const flatMap = require('unist-util-flatmap');

function flattenNestedLists() {
  return ast => {
    return flatMap(ast, node => {
      if (node.type !== 'list') return [node];
      const out = [];
      let latestContiguousList = Object.assign({}, node, {children: []});
      let changed = false;
      for (let listItem of node.children) {
        if (listItem.type !== 'listItem') throw new Error('malformed list');

        if (
          listItem.children.length > 0 &&
          listItem.children[0].type === 'paragraph'
        ) {
          latestContiguousList.children.push(
            Object.assign({}, listItem, {children: [listItem.children[0]]}),
          );
          changed = true;
        } else {
          latestContiguousList.children.push(
            Object.assign({}, listItem, {children: []}),
          );
          changed = true;
        }

        for (let i = 0, n = listItem.children.length; i < n; i++) {
          const nestedList = listItem.children[i];
          if (nestedList && nestedList.type === 'list') {
            if (changed) {
              out.push(latestContiguousList);
              latestContiguousList = Object.assign({}, node, {children: []});
              changed = false;
            }
            out.push(nestedList);
          }
        }
      }
      if (changed) {
        out.push(latestContiguousList);
      }
      return out;
    });
  };
}

module.exports = flattenNestedLists;
