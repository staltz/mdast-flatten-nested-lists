const test = require('tape');
const unified = require('unified');
const remarkParse = require('remark-parse');
const inspect = require('unist-util-inspect');
const flattenImageParagraphs = require('./index');

test('it lifts a nested list to the root level', t => {
  t.plan(2);

  const markdown = `
# Title

- First
  - a
    - aa
      - aaa
  - b
- Second
`;

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'First',
                    position: {
                      start: {line: 4, column: 3, offset: 12},
                      end: {line: 4, column: 8, offset: 17},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 4, column: 3, offset: 12},
                  end: {line: 4, column: 8, offset: 17},
                  indent: [],
                },
              },
              {
                type: 'list',
                ordered: false,
                start: null,
                loose: false,
                children: [
                  {
                    type: 'listItem',
                    loose: false,
                    checked: null,
                    children: [
                      {
                        type: 'paragraph',
                        children: [
                          {
                            type: 'text',
                            value: 'a',
                            position: {
                              start: {line: 5, column: 5, offset: 22},
                              end: {line: 5, column: 6, offset: 23},
                              indent: [],
                            },
                          },
                        ],
                        position: {
                          start: {line: 5, column: 5, offset: 22},
                          end: {line: 5, column: 6, offset: 23},
                          indent: [],
                        },
                      },
                      {
                        type: 'list',
                        ordered: false,
                        start: null,
                        loose: false,
                        children: [
                          {
                            type: 'listItem',
                            loose: false,
                            checked: null,
                            children: [
                              {
                                type: 'paragraph',
                                children: [
                                  {
                                    type: 'text',
                                    value: 'aa',
                                    position: {
                                      start: {line: 6, column: 7, offset: 30},
                                      end: {line: 6, column: 9, offset: 32},
                                      indent: [],
                                    },
                                  },
                                ],
                                position: {
                                  start: {line: 6, column: 7, offset: 30},
                                  end: {line: 6, column: 9, offset: 32},
                                  indent: [],
                                },
                              },
                              {
                                type: 'list',
                                ordered: false,
                                start: null,
                                loose: false,
                                children: [
                                  {
                                    type: 'listItem',
                                    loose: false,
                                    checked: null,
                                    children: [
                                      {
                                        type: 'paragraph',
                                        children: [
                                          {
                                            type: 'text',
                                            value: 'aaa',
                                            position: {
                                              start: {
                                                line: 7,
                                                column: 9,
                                                offset: 41,
                                              },
                                              end: {
                                                line: 7,
                                                column: 12,
                                                offset: 44,
                                              },
                                              indent: [],
                                            },
                                          },
                                        ],
                                        position: {
                                          start: {
                                            line: 7,
                                            column: 9,
                                            offset: 41,
                                          },
                                          end: {
                                            line: 7,
                                            column: 12,
                                            offset: 44,
                                          },
                                          indent: [],
                                        },
                                      },
                                    ],
                                    position: {
                                      start: {line: 7, column: 7, offset: 39},
                                      end: {line: 7, column: 12, offset: 44},
                                      indent: [],
                                    },
                                  },
                                ],
                                position: {
                                  start: {line: 7, column: 7, offset: 39},
                                  end: {line: 7, column: 12, offset: 44},
                                  indent: [],
                                },
                              },
                            ],
                            position: {
                              start: {line: 6, column: 5, offset: 28},
                              end: {line: 7, column: 12, offset: 44},
                              indent: [5],
                            },
                          },
                        ],
                        position: {
                          start: {line: 6, column: 5, offset: 28},
                          end: {line: 7, column: 12, offset: 44},
                          indent: [5],
                        },
                      },
                    ],
                    position: {
                      start: {line: 5, column: 3, offset: 20},
                      end: {line: 7, column: 12, offset: 44},
                      indent: [3, 3],
                    },
                  },
                  {
                    type: 'listItem',
                    loose: false,
                    checked: null,
                    children: [
                      {
                        type: 'paragraph',
                        children: [
                          {
                            type: 'text',
                            value: 'b',
                            position: {
                              start: {line: 8, column: 5, offset: 49},
                              end: {line: 8, column: 6, offset: 50},
                              indent: [],
                            },
                          },
                        ],
                        position: {
                          start: {line: 8, column: 5, offset: 49},
                          end: {line: 8, column: 6, offset: 50},
                          indent: [],
                        },
                      },
                    ],
                    position: {
                      start: {line: 8, column: 3, offset: 47},
                      end: {line: 8, column: 6, offset: 50},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 5, column: 3, offset: 20},
                  end: {line: 8, column: 6, offset: 50},
                  indent: [3, 3, 3],
                },
              },
            ],
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 8, column: 6, offset: 50},
              indent: [1, 1, 1, 1],
            },
          },
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'Second',
                    position: {
                      start: {line: 9, column: 3, offset: 53},
                      end: {line: 9, column: 9, offset: 59},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 9, column: 3, offset: 53},
                  end: {line: 9, column: 9, offset: 59},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 9, column: 1, offset: 51},
              end: {line: 9, column: 9, offset: 59},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 9, column: 9, offset: 59},
          indent: [1, 1, 1, 1, 1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 10, column: 1, offset: 60},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = flattenImageParagraphs()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'heading',
        depth: 1,
        children: [
          {
            type: 'text',
            value: 'Title',
            position: {
              start: {line: 2, column: 3, offset: 3},
              end: {line: 2, column: 8, offset: 8},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 1, offset: 1},
          end: {line: 2, column: 8, offset: 8},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'First',
                    position: {
                      start: {line: 4, column: 3, offset: 12},
                      end: {line: 4, column: 8, offset: 17},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 4, column: 3, offset: 12},
                  end: {line: 4, column: 8, offset: 17},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 4, column: 1, offset: 10},
              end: {line: 8, column: 6, offset: 50},
              indent: [1, 1, 1, 1],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 9, column: 9, offset: 59},
          indent: [1, 1, 1, 1, 1],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'a',
                    position: {
                      start: {line: 5, column: 5, offset: 22},
                      end: {line: 5, column: 6, offset: 23},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 5, column: 5, offset: 22},
                  end: {line: 5, column: 6, offset: 23},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 5, column: 3, offset: 20},
              end: {line: 7, column: 12, offset: 44},
              indent: [3, 3],
            },
          },
        ],
        position: {
          start: {line: 5, column: 3, offset: 20},
          end: {line: 8, column: 6, offset: 50},
          indent: [3, 3, 3],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'aa',
                    position: {
                      start: {line: 6, column: 7, offset: 30},
                      end: {line: 6, column: 9, offset: 32},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 6, column: 7, offset: 30},
                  end: {line: 6, column: 9, offset: 32},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 6, column: 5, offset: 28},
              end: {line: 7, column: 12, offset: 44},
              indent: [5],
            },
          },
        ],
        position: {
          start: {line: 6, column: 5, offset: 28},
          end: {line: 7, column: 12, offset: 44},
          indent: [5],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'aaa',
                    position: {
                      start: {line: 7, column: 9, offset: 41},
                      end: {line: 7, column: 12, offset: 44},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 7, column: 9, offset: 41},
                  end: {line: 7, column: 12, offset: 44},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 7, column: 7, offset: 39},
              end: {line: 7, column: 12, offset: 44},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 7, column: 7, offset: 39},
          end: {line: 7, column: 12, offset: 44},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'b',
                    position: {
                      start: {line: 8, column: 5, offset: 49},
                      end: {line: 8, column: 6, offset: 50},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 8, column: 5, offset: 49},
                  end: {line: 8, column: 6, offset: 50},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 8, column: 3, offset: 47},
              end: {line: 8, column: 6, offset: 50},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 5, column: 3, offset: 20},
          end: {line: 8, column: 6, offset: 50},
          indent: [3, 3, 3],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'paragraph',
                children: [
                  {
                    type: 'text',
                    value: 'Second',
                    position: {
                      start: {line: 9, column: 3, offset: 53},
                      end: {line: 9, column: 9, offset: 59},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 9, column: 3, offset: 53},
                  end: {line: 9, column: 9, offset: 59},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 9, column: 1, offset: 51},
              end: {line: 9, column: 9, offset: 59},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 4, column: 1, offset: 10},
          end: {line: 9, column: 9, offset: 59},
          indent: [1, 1, 1, 1, 1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 10, column: 1, offset: 60},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output looks good');
});

test('edge case when flattening lists', t => {
  const markdown = '- \n-';

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 1, column: 1, offset: 0},
              end: {line: 1, column: 3, offset: 2},
              indent: [],
            },
          },
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 2, column: 1, offset: 3},
              end: {line: 2, column: 2, offset: 4},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 1, column: 1, offset: 0},
          end: {line: 2, column: 2, offset: 4},
          indent: [1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 2, column: 2, offset: 4},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = flattenImageParagraphs()(actualInput);

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  // using expectedInput because actualInput is mutated by now
  t.deepEquals(actualOutput, expectedInput, 'output is same as input');

  t.end();
});

test('edge case with nested list when flattening lists', t => {
  const markdown = '- \n  - \n- \n  - \n    - ';

  const actualInput = unified()
    .use(remarkParse, {commonmark: true})
    .parse(markdown);

  //console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: 'root',
    children: [
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'list',
                ordered: false,
                start: null,
                loose: false,
                children: [
                  {
                    type: 'listItem',
                    loose: false,
                    checked: null,
                    children: [],
                    position: {
                      start: {line: 2, column: 3, offset: 5},
                      end: {line: 2, column: 5, offset: 7},
                      indent: [],
                    },
                  },
                ],
                position: {
                  start: {line: 2, column: 3, offset: 5},
                  end: {line: 2, column: 5, offset: 7},
                  indent: [],
                },
              },
            ],
            position: {
              start: {line: 1, column: 1, offset: 0},
              end: {line: 2, column: 5, offset: 7},
              indent: [1],
            },
          },
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [
              {
                type: 'list',
                ordered: false,
                start: null,
                loose: false,
                children: [
                  {
                    type: 'listItem',
                    loose: false,
                    checked: null,
                    children: [
                      {
                        type: 'list',
                        ordered: false,
                        start: null,
                        loose: false,
                        children: [
                          {
                            type: 'listItem',
                            loose: false,
                            checked: null,
                            children: [],
                            position: {
                              start: {line: 5, column: 5, offset: 20},
                              end: {line: 5, column: 7, offset: 22},
                              indent: [],
                            },
                          },
                        ],
                        position: {
                          start: {line: 5, column: 5, offset: 20},
                          end: {line: 5, column: 7, offset: 22},
                          indent: [],
                        },
                      },
                    ],
                    position: {
                      start: {line: 4, column: 3, offset: 13},
                      end: {line: 5, column: 7, offset: 22},
                      indent: [3],
                    },
                  },
                ],
                position: {
                  start: {line: 4, column: 3, offset: 13},
                  end: {line: 5, column: 7, offset: 22},
                  indent: [3],
                },
              },
            ],
            position: {
              start: {line: 3, column: 1, offset: 8},
              end: {line: 5, column: 7, offset: 22},
              indent: [1, 1],
            },
          },
        ],
        position: {
          start: {line: 1, column: 1, offset: 0},
          end: {line: 5, column: 7, offset: 22},
          indent: [1, 1, 1, 1],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 5, column: 7, offset: 22},
    },
  };
  t.deepEquals(actualInput, expectedInput, 'input looks good');

  const actualOutput = flattenImageParagraphs()(actualInput);

  //console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: 'root',
    children: [
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 1, column: 1, offset: 0},
              end: {line: 2, column: 5, offset: 7},
              indent: [1],
            },
          },
        ],
        position: {
          start: {line: 1, column: 1, offset: 0},
          end: {line: 5, column: 7, offset: 22},
          indent: [1, 1, 1, 1],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 2, column: 3, offset: 5},
              end: {line: 2, column: 5, offset: 7},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 2, column: 3, offset: 5},
          end: {line: 2, column: 5, offset: 7},
          indent: [],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 3, column: 1, offset: 8},
              end: {line: 5, column: 7, offset: 22},
              indent: [1, 1],
            },
          },
        ],
        position: {
          start: {line: 1, column: 1, offset: 0},
          end: {line: 5, column: 7, offset: 22},
          indent: [1, 1, 1, 1],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 4, column: 3, offset: 13},
              end: {line: 5, column: 7, offset: 22},
              indent: [3],
            },
          },
        ],
        position: {
          start: {line: 4, column: 3, offset: 13},
          end: {line: 5, column: 7, offset: 22},
          indent: [3],
        },
      },
      {
        type: 'list',
        ordered: false,
        start: null,
        loose: false,
        children: [
          {
            type: 'listItem',
            loose: false,
            checked: null,
            children: [],
            position: {
              start: {line: 5, column: 5, offset: 20},
              end: {line: 5, column: 7, offset: 22},
              indent: [],
            },
          },
        ],
        position: {
          start: {line: 5, column: 5, offset: 20},
          end: {line: 5, column: 7, offset: 22},
          indent: [],
        },
      },
    ],
    position: {
      start: {line: 1, column: 1, offset: 0},
      end: {line: 5, column: 7, offset: 22},
    },
  };

  t.deepEquals(actualOutput, expectedOutput, 'output is correct');

  t.end();
});
